/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
package org.eclipse.openk.portal.common;

import org.eclipse.openk.portal.common.util.ResourceLoaderBase;
import org.junit.Test;
import javax.servlet.ServletException;

public class InitUserCacheJobTest extends ResourceLoaderBase {

    @Test
    public void testInit() throws ServletException {
        InitUserCacheJob job = new InitUserCacheJob();
        job.init();
        // Test only covers Code!
    }

}
