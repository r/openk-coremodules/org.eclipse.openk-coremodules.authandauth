/**
 ******************************************************************************
 * Copyright © 2017-2018 PTA GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 *
 *     http://www.eclipse.org/legal/epl-v10.html
 *
 ******************************************************************************
 */
package org.eclipse.openk.portal.health.impl;

import org.junit.Before;
import org.junit.Test;
import org.powermock.reflect.Whitebox;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class KeyCloakPresentHealthCheckTest {
    private KeyCloakPresentHealthCheck hc;

    @Before
    public void init() {
        hc = new KeyCloakPresentHealthCheck() {
            public boolean checkit;

            @Override
            protected boolean checkit() {
                return checkit;
            }
        };
    }

    @Test
    public void testOk() throws Exception {
        Whitebox.setInternalState(hc, "checkit", true);
        assertTrue(hc.check().isHealthy());
    }

    @Test
    public void testNok() throws Exception {
        Whitebox.setInternalState(hc, "checkit", false);
        assertFalse(hc.check().isHealthy());
    }

}
