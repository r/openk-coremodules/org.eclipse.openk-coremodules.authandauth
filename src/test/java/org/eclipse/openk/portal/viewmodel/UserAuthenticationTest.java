/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
package org.eclipse.openk.portal.viewmodel;


import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.eclipse.openk.portal.common.JsonGeneratorBase;
import org.eclipse.openk.portal.common.util.ResourceLoaderBase;
import org.junit.Test;

public class UserAuthenticationTest extends ResourceLoaderBase {

    @Test
    public void testStructureAgainstJson() {
        String json = super.loadStringFromResource("testUserAuthentication.json");
        UserAuthentication ua = JsonGeneratorBase.getGson().fromJson(json, UserAuthentication.class);
        assertTrue(ua.getId() == 1 );
        assertTrue(ua.getUsername().equals("Pedro"));
        assertTrue(ua.getPassword().equals("pwd"));
        assertTrue(ua.getName().equals("Pedro Pepito Sanchez"));
        assertFalse(ua.isSpecialUser());
        assertTrue(ua.isSelected());
    }

    @Test
    public void testSetters() {
        UserAuthentication ua = new UserAuthentication();
        ua.setId(1);
        ua.setName("Vamos-Namos");
        ua.setPassword("passoporto");
        ua.setUsername("VNAMOS");
        ua.setSpecialUser(true);
        ua.setSelected(true);
    }

}
