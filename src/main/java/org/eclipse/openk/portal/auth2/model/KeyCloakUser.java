/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
package org.eclipse.openk.portal.auth2.model;

import java.util.List;
import java.util.Map;

public class KeyCloakUser {
    private String id;
    private long createdTimestamp;
    private String username;
    private boolean enabled;
    private boolean totp;
    private boolean emailVerified;
    private String firstName;
    private String lastName;
    private String name;
    private List<String> allRoles;
    private List<String> realmRoles;
    private Map<String, List<String>> clientRoles;

    private List<String> disableableCredentialTypes;
    private List<String> requiredActions;
    private KeyCloakUserAccess access;

    public String getId() { return id; }
    public void setId(String id) { this.id = id; }

    public long getCreatedTimestamp() { return createdTimestamp; }
    public void setCreatedTimestamp(long createdTimestamp) { this.createdTimestamp = createdTimestamp; }

    public String getUsername() { return username; }
    public void setUsername(String username) { this.username = username; }

    public void setTotp(boolean totp) { this.totp = totp; }

    public void setEmailVerified(boolean emailVerified) { this.emailVerified = emailVerified; }

    public String getFirstName() { return firstName; }
    public void setFirstName(String firstName) { this.firstName = firstName; }

    public String getLastName() { return lastName; }
    public void setLastName(String lastName) { this.lastName = lastName; }

    public List<String> getDisableableCredentialTypes() {
        return disableableCredentialTypes;
    }
    public void setDisableableCredentialTypes(List<String> disableableCredentialTypes) { this.disableableCredentialTypes = disableableCredentialTypes; }

    public List<String> getRequiredActions() {
        return requiredActions;
    }
    public void setRequiredActions(List<String> requiredActions) { this.requiredActions = requiredActions; }

    public KeyCloakUserAccess getAccess() { return access; }
    public void setAccess(KeyCloakUserAccess access) { this.access = access; }

    public boolean isEnabled() {
        return enabled;
    }

    public boolean isTotp() {
        return totp;
    }

    public boolean isEmailVerified() {
        return emailVerified;
    }

    public List<String> getAllRoles() {
        return allRoles;
    }

    public void setAllRoles(List<String> allRoles) {
        this.allRoles = allRoles;
    }

    public List<String> getRealmRoles() {
        return realmRoles;
    }

    public void setRealmRoles(List<String> realmRoles) {
        this.realmRoles = realmRoles;
    }

    public Map<String, List<String>> getClientRoles() {
        return clientRoles;
    }

    public void setClientRoles(Map<String, List<String>> clientRoles) {
        this.clientRoles = clientRoles;
    }

    public void setName() {
        if (lastName!= null && !lastName.isEmpty()){
            this.name = firstName+ " " + lastName;
        } else {
            this.name = firstName;
        }
    }
}
