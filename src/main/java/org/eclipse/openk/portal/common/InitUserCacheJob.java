/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
package org.eclipse.openk.portal.common;

import java.util.Timer;
import java.util.TimerTask;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import org.apache.log4j.Logger;

public class InitUserCacheJob extends HttpServlet {

    private static final long serialVersionUID = 8671326157376987023L;

    private static final Logger LOGGER = Logger.getLogger(InitUserCacheJob.class.getName());


    @Override
    public void init() throws ServletException {
        LOGGER.debug("InitUserCacheJob called");
        TimerTask timerTask = new UserCacheTimerTask();
        Timer timer = new Timer();
        timer.scheduleAtFixedRate(timerTask, 500, BackendConfig.getInstance().getReloadUsersInSec()*1000L);
    }
}
