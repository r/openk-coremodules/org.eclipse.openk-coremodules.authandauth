/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
package org.eclipse.openk.portal.common;

import org.apache.log4j.Logger;
import org.eclipse.openk.portal.common.util.ResourceLoaderBase;
import org.eclipse.openk.portal.viewmodel.UserModule;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

public class InitBackendConfig extends HttpServlet {

    private static final long serialVersionUID = -7882117179312471533L;

    private static final Logger LOGGER = Logger.getLogger(InitBackendConfig.class.getName());
    private String backendConfigFile;
    private String moduleConfigFile;

    @Override
    public void init() throws ServletException {
        String environment = getServletContext().getInitParameter("OK_PORTAL_ENVIRONMENT");
        setConfigFiles(environment);
    }

    private void setConfigFiles(String environment ) {
        String env = (environment == null ? "Production": environment);

        switch (env){
            case "ExternalConfig":
                loadExternalConfig();
                break;
            case "DevLocal":
                backendConfigFile ="backendConfigDevLocal.json";
                moduleConfigFile ="moduleConfigDevLocal.json";
                break;
            case "DevServer":
                backendConfigFile ="backendConfigDevServer.json";
                moduleConfigFile ="moduleConfigDevServer.json";
                break;
            case "Custom":
                backendConfigFile ="backendConfigCustom.json";
                moduleConfigFile ="moduleConfigCustom.json";
                break;
            case "Docker":
                backendConfigFile ="backendConfigDocker.json";
                moduleConfigFile ="moduleConfigDocker.json";
                break;
            default:
                setDefault();
        }

        BackendConfig.setConfigFileName(backendConfigFile);
        UserModule.setConfigFileName(moduleConfigFile);
        LOGGER.info("Portal backendendenviroment is: " +environment+ ". Setting backendConfig accordingly to: "+ backendConfigFile);
        LOGGER.info("Portal backendendenviroment is: " +environment+ ". Setting moduleConfig accordingly to: "+ moduleConfigFile);
    }

    private void setDefault() {
        backendConfigFile ="backendConfigProduction.json";
        moduleConfigFile ="moduleConfigProduction.json";
    }

    private void loadExternalConfig() {
        Context ctx = null;
        try {
            ctx = new InitialContext();
            backendConfigFile = (String) ctx.lookup("java:comp/env/configurationPathBackend");
            moduleConfigFile = (String) ctx.lookup("java:comp/env/configurationPathModules");
        } catch (NamingException e) {
            LOGGER.error("Error while loadExternalConfig", e);
        }
    }

}
