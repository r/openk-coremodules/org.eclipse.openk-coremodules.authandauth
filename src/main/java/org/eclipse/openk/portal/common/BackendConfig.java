/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
package org.eclipse.openk.portal.common;

import org.eclipse.openk.portal.common.util.ResourceLoaderBase;

public class BackendConfig {
    private static String configFileName = "backendConfigDevLocal.json";
    private Integer internalSessionLengthMillis;
    private Integer reloadUsersInSec;
    private String authServerUrl;
    private String keycloakRealm;
    private String keycloakClient;
    private String keycloakAdmin;
    private String keycloakPW;
    private Integer maxLoadUsers;

    private static BackendConfig instance;

    private BackendConfig() {}

    public static synchronized BackendConfig getInstance() {
        if (instance == null) {
            String jsonConfig = loadJsonConfig();
            instance = JsonGeneratorBase.getGson().fromJson(jsonConfig, BackendConfig.class);
        }

        return instance;
    }

    private static String loadJsonConfig() {
        ResourceLoaderBase resourceLoaderBase = new ResourceLoaderBase();
        String jsonConfig = resourceLoaderBase.loadStringFromResource(configFileName);
        if (jsonConfig == null || jsonConfig.isEmpty()) {
            jsonConfig = resourceLoaderBase.loadFromPath(configFileName);
        }
        return jsonConfig;
    }

    public Integer getInternalSessionLengthMillis() { return internalSessionLengthMillis; }

    public Integer getReloadUsersInSec() { return reloadUsersInSec; }

    public String getAuthServerUrl() {
        return authServerUrl;
    }

    public String getKeycloakClient() {
        return keycloakClient;
    }

    public String getKeycloakAdmin() {
        return keycloakAdmin;
    }

    public String getKeycloakPW() {
        return keycloakPW;
    }

    public String getKeycloakRealm() {
        return keycloakRealm;
    }

    public Integer getMaxLoadUsers() {
        return maxLoadUsers;
    }

    public static String getConfigFileName() {
        return configFileName;
    }

    public static void setConfigFileName(String configFileName) {
        BackendConfig.configFileName = configFileName;
    }
}
