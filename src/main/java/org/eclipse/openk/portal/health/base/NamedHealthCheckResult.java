/*
 ******************************************************************************
 * Copyright © 2017-2018 PTA GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 *
 *     http://www.eclipse.org/legal/epl-v10.html
 *
 ******************************************************************************
 */
package org.eclipse.openk.portal.health.base;

import com.google.gson.GsonBuilder;

import java.util.List;

public class NamedHealthCheckResult {
    private String name;
    private Result result;

    public NamedHealthCheckResult( String name, Result result ) {
        this.name = name;
        this.result = result;
    }

    public String toJson() {
        StringBuffer buffer = new StringBuffer(); // NOSONAR - _fd we don't a new dependency because of this
        buffer.append("\""+name+"\":");
        buffer.append(new GsonBuilder().disableHtmlEscaping().create().toJson(result));
        return buffer.toString();
    }

    public static String toJson(List<NamedHealthCheckResult> resultArray ) {
        StringBuilder builder = new StringBuilder();
        builder.append('{');
        for( int i=0; i < resultArray.size(); i++ ) {
            builder.append(resultArray.get(i).toJson());
            if( i+1 < resultArray.size() ) {
                builder.append(',');
            }
        }
        return builder.append('}').toString();
    }

    public String getName() {
        return name;
    }

    public Result getResult() {
        return result;
    }
}
