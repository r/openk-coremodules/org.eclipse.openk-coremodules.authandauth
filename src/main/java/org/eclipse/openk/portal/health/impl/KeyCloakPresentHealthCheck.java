/*
 ******************************************************************************
 * Copyright © 2017-2018 PTA GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 *
 *     http://www.eclipse.org/legal/epl-v10.html
 *
 ******************************************************************************
 */
package org.eclipse.openk.portal.health.impl;

import org.eclipse.openk.portal.auth2.util.JwtHelper;
import org.eclipse.openk.portal.common.BackendConfig;
import org.eclipse.openk.portal.exceptions.PortalInternalServerError;
import org.eclipse.openk.portal.health.base.HealthCheck;
import org.eclipse.openk.portal.health.base.Result;

public class KeyCloakPresentHealthCheck extends HealthCheck {

    protected boolean checkit() throws PortalInternalServerError {
        return JwtHelper.serviceAvailable();
    }

    @Override
    protected Result check() throws Exception {
        if( checkit() ) {
            return Result.healthy();
        }
        else {
            BackendConfig gc = BackendConfig.getInstance();
            String msg = gc.getAuthServerUrl() + "/" + gc.getKeycloakRealm();
            return Result.unhealthy("KeyCloak-Service not available: "+msg);
        }
    }
}
