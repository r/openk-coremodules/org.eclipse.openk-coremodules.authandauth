/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
package org.eclipse.openk.portal.viewmodel;

import org.eclipse.openk.portal.common.JsonGeneratorBase;
import org.eclipse.openk.portal.common.util.ResourceLoaderBase;

public class UserModule {

    private static String configFileName = "moduleConfigDevLocal.json";
    private String name;
    private Integer cols;
    private Integer rows;
    private String color;
    private String link;
    private String pictureLink;
    private String requiredRole;

    private static UserModule[] instance;

    public static synchronized UserModule[] getInstance() {
        if (instance == null) {
            String jsonConfig = loadJsonConfig();
            instance = JsonGeneratorBase.getGson().fromJson(jsonConfig, UserModule[].class);
        }

        return instance;
    }

    private static String loadJsonConfig() {
        ResourceLoaderBase resourceLoaderBase = new ResourceLoaderBase();
        String jsonConfig = resourceLoaderBase.loadStringFromResource(configFileName);
        if (jsonConfig == null || jsonConfig.isEmpty()) {
            jsonConfig = resourceLoaderBase.loadFromPath(configFileName);
        }
        return jsonConfig;
    }

    public String getModuleName() { return name; }

    public void setModuleName(String name) { this.name = name; }

    public Integer getCols() { return cols; }

    public void setCols(Integer cols) { this.cols = cols; }

    public Integer getRows() { return rows; }

    public void setRows(Integer rows) { this.rows = rows; }

    public String getColor() { return color; }

    public void setColor(String color) { this.color = color; }

    public String getLink() { return link; }

    public void setLink(String link) { this.link = link; }

    public String getPictureLink() { return pictureLink; }

    public void setPictureLink(String pictureLink) { this.pictureLink = pictureLink; }

    public String getRequiredRole() { return requiredRole; }

    public void setRequiredRole(String role) { this.requiredRole = role; }

    public static String getConfigFileName() {
        return configFileName;
    }

    public static void setConfigFileName(String configFileName) {
        UserModule.configFileName = configFileName;
    }

}
