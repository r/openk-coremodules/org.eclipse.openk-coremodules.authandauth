/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
package org.eclipse.openk.portal.viewmodel;

import java.util.ArrayList;
import java.util.List;
import org.eclipse.openk.portal.auth2.model.KeyCloakUser;

public class UserCache {

  private static final UserCache USERCACHE_INSTANCE = new UserCache();
  private final List<KeyCloakUser> keyCloakUsers = new ArrayList<>();

  private UserCache(){
  }

  public static UserCache getInstance() {
      return USERCACHE_INSTANCE;
  }

  public List<KeyCloakUser> getKeyCloakUsers() {
    synchronized (keyCloakUsers){
      return keyCloakUsers;
    }
  }

  public void setKeyCloakUsers(List<KeyCloakUser> keyCloakUsers) {
    synchronized (this.keyCloakUsers){
      this.keyCloakUsers.clear();
      this.keyCloakUsers.addAll(keyCloakUsers);
    }
  }
}
