/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
package org.eclipse.openk.portal.exceptions;

public abstract class PortalException extends Exception { // NOSONAR
    public PortalException() {
        super();
    }

    public PortalException(String message) {
        super(message);
    }

    public PortalException(String message, Throwable throwable) {
        super(message, throwable);
    }

    public abstract int getHttpStatus();
}
